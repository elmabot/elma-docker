FROM node:15
# this needs to be declared for the install anyways
ENV NODE_ENV=development

WORKDIR /app

# copy dependencies first and install
COPY ["package.json", "package-lock.json*", "./"]
RUN npm install

# copy code
COPY . .

RUN npm install -g nodemon

CMD npm start

